package de.heliosdevelopment.owncommands.command;

import de.heliosdevelopment.owncommands.OwnCommands;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {

    private final CommandHandler commandHandler;

    public CommandListener(OwnCommands plugin, CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        String name = event.getMessage().substring(1, event.getMessage().length());
        System.out.println(name);
        OwnCommand command = commandHandler.getCommand(name);
        if (command != null) {
            Player player = event.getPlayer();
            if (command.getPermission().trim() != "" && command.getPermission() != null)
                if (!player.hasPermission(command.getPermission())) {
                    player.sendMessage("§7[§cOwnCommands§7] Du hast keine Rechte um diesen Befehl nutzen zu können!");
                    return;
                }
            for (String message : command.getOutput()) {
                player.sendMessage(message);
            }
            event.setCancelled(true);

        }
    }
}
