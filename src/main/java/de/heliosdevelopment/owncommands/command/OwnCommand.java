package de.heliosdevelopment.owncommands.command;

import org.bukkit.ChatColor;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class OwnCommand {

    private final String name;
    private final String permission;
    private final String description;
    private final List<String> output;

    public OwnCommand(String name, String permission, String description, List<String> output){
        this.name = name;
        this.permission = permission;
        this.output = new ArrayList<>();
        for(String s : output)
            this.output.add(ChatColor.translateAlternateColorCodes('&', s));
        this.description = description;

    }

    public List<String> getOutput() {
        return output;
    }

    public String getPermission() {
        return permission;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
