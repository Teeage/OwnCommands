package de.heliosdevelopment.owncommands.command;

import java.util.ArrayList;
import java.util.List;

public class CommandHandler {

    private final List<OwnCommand> commands;

    public CommandHandler(List<OwnCommand> commands) {
        this.commands = commands;
    }

    public List<OwnCommand> getCommands() {
        return commands;
    }

    public OwnCommand getCommand(String name) {
        for (OwnCommand command : commands)
            if (command.getName().equalsIgnoreCase(name))
                return command;
        return null;
    }
}
