package de.heliosdevelopment.owncommands.command;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class CommandLoader {

    public List<OwnCommand> loadCommands(FileConfiguration cfg) {
        List<OwnCommand> commands = new ArrayList<>();
        for (String string : cfg.getConfigurationSection("commands").getKeys(false)) {
            commands.add(new OwnCommand(cfg.getString("commands." + string + ".name"), cfg.getString("commands." + string + ".permission"), cfg.getString("commands." + string + ".description"), cfg.getStringList("commands." + string + ".output")));
        }
        return commands;
    }
}
