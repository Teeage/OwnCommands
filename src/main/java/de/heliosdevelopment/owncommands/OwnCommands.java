package de.heliosdevelopment.owncommands;

import de.heliosdevelopment.owncommands.command.CommandHandler;
import de.heliosdevelopment.owncommands.command.CommandListener;
import de.heliosdevelopment.owncommands.command.CommandLoader;
import de.heliosdevelopment.owncommands.command.OwnCommand;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class OwnCommands extends JavaPlugin {

    @Override
    public void onEnable() {
        getConfig().options().copyDefaults(true);
        saveConfig();
        List<OwnCommand> commands = new CommandLoader().loadCommands(getConfig());
        CommandHandler commandHandler = new CommandHandler(commands);
        new CommandListener(this, commandHandler);
    }
}
